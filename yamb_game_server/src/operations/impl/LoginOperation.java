/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operations.impl;

import domain.Player;
import java.util.Collections;
import java.util.List;
import operations.GenericOperations;
import request.RequestOperation;

/**
 *
 * @author anjak
 */
public class LoginOperation extends GenericOperations<Player, Player> {

    private Player player;

    @Override
    protected void validate(Player entity) throws Exception {
        if (entity.getUsername() == null || entity.getPassword() == null) {
            throw new RuntimeException("Username and password are required!");
        }
    }

    @Override
    public Player getResult() {
        return player;
    }

    @Override
    public RequestOperation getSupportedOperation() {
        return RequestOperation.LOGIN;
    }

    @Override
    protected void execute(Player entity) throws Exception {
        player = databaseBroker.findRecord(entity)
                .map(Player.class::cast)
                .orElseThrow(() -> new RuntimeException("Pogresno korisnicko ime"));
        if (!player.getPassword().equals(entity.getPassword())) {
            throw new RuntimeException("Pogresna sifra");
        };
    }

    @Override
    public List<Player> getResultList() {
        return Collections.EMPTY_LIST;
    }

}
