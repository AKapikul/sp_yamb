/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operations.impl;

import domain.DomainEntity;
import domain.Player;
import java.util.List;
import operations.GenericOperations;
import request.RequestOperation;

/**
 *
 * @author anjak
 */
public class GetTop10PlayersOperation extends GenericOperations<DomainEntity, DomainEntity>{
    
    private List<DomainEntity> resultList;

    @Override
    protected void validate(DomainEntity entity) throws Exception {
        
    }

    @Override
    public DomainEntity getResult() {
        return null;
    }

    @Override
    public List<DomainEntity> getResultList() {
        return resultList;
    }

    @Override
    public RequestOperation getSupportedOperation() {
        return RequestOperation.GET_TOP_10_PLAYERS;
    }

    @Override
    protected void execute(DomainEntity entity) throws Exception {
        resultList = databaseBroker.findRecordWithLimit(entity, 10);
    }
   
    
    
}
