/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operations.impl;

import db.impl.DbBrokerImpl;
import domain.ScorePerGames;
import java.util.List;
import operations.GenericOperations;
import request.RequestOperation;

/**
 *
 * @author anjak
 */
public class ScorePerGamesOperation extends GenericOperations<ScorePerGames, ScorePerGames> {

    private List<ScorePerGames> resultList;

    @Override
    protected void validate(ScorePerGames entity) throws Exception {
    }

    @Override
    public ScorePerGames getResult() {
        return null;
    }

    @Override
    public List<ScorePerGames> getResultList() {
        return resultList;
    }

    @Override
    public RequestOperation getSupportedOperation() {
        return RequestOperation.GET_SCORE_PER_GAMES;
    }

    @Override
    protected void execute(ScorePerGames entity) throws Exception {
        resultList = ((DbBrokerImpl) databaseBroker)
                .findScorePerGames(entity);
    }

}
