/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operations.impl;

import domain.Player;
import java.util.Collections;
import java.util.List;
import operations.GenericOperations;
import request.RequestOperation;

/**
 *
 * @author anjak
 */
public class SignupOperation extends GenericOperations<Player, Player> {

    @Override
    protected void validate(Player entity) throws Exception {
        if (entity.getUsername() == null || entity.getUsername().isEmpty()) {
            throw new Exception("Username is required!");
        }
        if (entity.getPassword() == null || entity.getPassword().isEmpty()) {
            throw new Exception("Password is required!");
        }
    }

    @Override
    public Player getResult() {
        return null;
    }

    @Override
    public RequestOperation getSupportedOperation() {
        return RequestOperation.SIGNUP;
    }

    @Override
    protected void execute(Player entity) throws Exception {
        boolean exists = databaseBroker.findRecord(new Player(entity.getUsername()))
                .map(Player.class::cast)
                .isPresent();

        if (!exists) {
            databaseBroker.insertRecord(entity);
        } else {
            throw new RuntimeException("Username already exists!");
        }
    }
    
    @Override
    public List<Player> getResultList() {
        return Collections.EMPTY_LIST;
    }

}
