/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operations.impl;

import domain.Player;
import java.util.Collections;
import java.util.List;
import operations.GenericOperations;
import request.RequestOperation;

/**
 *
 * @author anjak
 */
public class UpdatePlayerOperation extends GenericOperations<Player, Player> {

    private Player player;

    @Override
    protected void validate(Player entity) throws Exception {
        if (entity.getId() == -1L) {
            throw new RuntimeException("Player not exists!");
        }
    }

    @Override
    public Player getResult() {
        return player;
    }

    @Override
    public RequestOperation getSupportedOperation() {
        return RequestOperation.UPDATE_PLAYER;
    }

    @Override
    protected void execute(Player entity) throws Exception {
        this.player = databaseBroker.findRecord(entity)
                .map(Player.class::cast)
                .orElseThrow(() -> new RuntimeException("Pogresno korisnicko ime"));

        if (entity.getFirstname() != null) {
            this.player.setFirstname(entity.getFirstname());
        }

        if (entity.getLastname() != null) {
            this.player.setLastname(entity.getLastname());
        }

        if (entity.getTopScore() > 0) {
            this.player.setTopScore(entity.getTopScore());
        }

        if (entity.getPassword() != null) {
            this.player.setPassword(entity.getPassword());
        }

        if (entity.getUsername() != null) {
            this.player.setUsername(entity.getUsername());
        }

        databaseBroker.updateRecord(this.player);
    }

    @Override
    public List<Player> getResultList() {
        return Collections.EMPTY_LIST;
    }

}
