/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operations;

import db.DbBroker;
import db.DbConnection;
import db.impl.DbBrokerImpl;
import domain.DomainEntity;
import java.util.List;
import request.RequestOperation;

/**
 *
 * @author anjak
 */
public abstract class GenericOperations<T extends DomainEntity, R> {

    protected DbBroker databaseBroker;

    public GenericOperations() {
        databaseBroker = new DbBrokerImpl();
    }

    protected abstract void validate(T entity) throws Exception;

    public abstract R getResult();
    
    public abstract List<R> getResultList();

    public abstract RequestOperation getSupportedOperation();

    private void startTransaction() throws Exception {

    }

    protected abstract void execute(T entity) throws Exception;

    private void commitTransaction() throws Exception {
        DbConnection.getInstance().commitTransaction();
    }

    private void rollbackTransaction() throws Exception {
        DbConnection.getInstance().rollbackTransaction();
    }

    public final void templateExecute(T entity) throws Exception {
        try {
            validate(entity);
            try {
                startTransaction();
                execute(entity);
                commitTransaction();
            } catch (Exception e) {
                rollbackTransaction();
                System.err.println(e);
                throw e;
            }
        } catch (Exception e) {
            System.err.println(e);
            throw e;
        }
    }

}
