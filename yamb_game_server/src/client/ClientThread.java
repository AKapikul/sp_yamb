/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import operations.GenericOperations;
import operations.OperationFinder;
import request.Request;
import request.RequestOperation;
import response.Response;
import response.ResponseStatus;

/**
 *
 * @author anjak
 */
public class ClientThread extends Thread {

    private final Socket socket;

    public ClientThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            handleRequest();
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println(ex);
        }
    }

    private void handleRequest() throws IOException, ClassNotFoundException {
        while (!isInterrupted()) {
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            Request request = (Request) in.readObject();
            Response response = new Response();
            System.out.println("Operation: " + request.getOperation());

            try {
                if (request.getOperation().equals(RequestOperation.EXIT_APP)) {
                    disconnectClient();
                } else {
                    GenericOperations operation = OperationFinder.findOperation(request.getOperation());
                    operation.templateExecute(request.getData());
                    response.setStatus(ResponseStatus.SUCCESS);
                    if (request.getOperation().equals(RequestOperation.GET_TOP_10_PLAYERS)) {
                        response.setPayload(operation.getResultList());
                    } else if (request.getOperation().equals(RequestOperation.GET_SCORE_PER_GAMES)) {
                        response.setPayload(operation.getResultList());
                    } else {
                        response.setPayload(operation.getResult());
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                response.setStatus(ResponseStatus.ERROR);
                response.setException(ex);
            }

            if (!request.getOperation().equals(RequestOperation.EXIT_APP)) {
                sendResponse(response);
            }
        }
    }

    private void sendResponse(Response response) throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
        out.writeObject(response);
    }

    private void disconnectClient() {
        try {
            socket.close();
            interrupt();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
