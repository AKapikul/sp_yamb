/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package communication;

import client.ClientThread;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author anjak
 */
public class CommunicationSocket {
      private final boolean active = true;

    public void startServer() throws Exception {
        Class.forName("operations.OperationFinder");
        ServerSocket ss = new ServerSocket(9000);
        System.out.println("Start connection");
        while (active) {
            Socket socket = ss.accept();
            System.out.println("Accept socket");
            ClientThread clientThread = new ClientThread(socket);
            clientThread.start();
        }
    }

}
