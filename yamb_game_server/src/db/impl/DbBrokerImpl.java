/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db.impl;

import db.DbBroker;
import db.DbConnection;
import domain.DomainEntity;
import domain.ScorePerGames;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author anjak
 */
public class DbBrokerImpl implements DbBroker{
    
    @Override
    public List<DomainEntity> getAllRecord(DomainEntity entity) throws SQLException {
        List<DomainEntity> objects = new LinkedList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM ").append(entity.getTableName());
        String query = sb.toString();
        System.out.println(query);
        PreparedStatement statement = DbConnection.getInstance().getConnection().prepareStatement(query);
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            objects.add(entity.getNewRecord(rs));
        }
        return objects;
    }

    @Override
    public Optional<DomainEntity> findRecord(DomainEntity entity) throws SQLException {
        String query = "SELECT * FROM " + entity.getTableName() + " WHERE " + entity.getWhereCondition();
        System.out.println(query);
        Statement st = DbConnection.getInstance().getConnection().prepareStatement(query);
        ResultSet rs = st.executeQuery(query);
        boolean signal = rs.next();
        if (signal == true) {
            return Optional.of(entity.getNewRecord(rs));
        }
        return Optional.ofNullable(null);
    }

    @Override
    public List<DomainEntity> findRecords(DomainEntity entity, DomainEntity parent) throws SQLException {
        List<DomainEntity> objects = new LinkedList<>();
        String query = "SELECT * FROM " + entity.getTableName() + " WHERE " + parent.getWhereCondition();
        System.out.println(query);
        PreparedStatement statement = DbConnection.getInstance().getConnection().prepareStatement(query);
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            objects.add(entity.getNewRecord(rs));
        }
        return objects;
    }

    @Override
    public Long insertRecord(DomainEntity entity) throws SQLException {
        System.out.println("Insert: " + entity);
        String upit = "INSERT INTO " + entity.getTableName() + " (" + entity.getAtributeNames() + ") VALUES (" + entity.getAtributeValues() + ")";
        return executeUpdate(upit);
    }

    @Override
    public void deleteRecord(DomainEntity entity) throws SQLException {
        String upit = "DELETE FROM " + entity.getTableName() + " WHERE " + entity.getWhereCondition();
        executeUpdate(upit);
    }

    @Override
    public void updateRecord(DomainEntity entity, DomainEntity entityId) throws SQLException {
        String upit = "UPDATE " + entity.getTableName() + " SET " + entity.setAtributeValues() + " WHERE " + entityId.getWhereCondition();
        executeUpdate(upit);

    }

    @Override
    public void updateRecord(DomainEntity entity) throws SQLException {
        String upit = "UPDATE " + entity.getTableName() + " SET " + entity.setAtributeValues() + " WHERE " + entity.getWhereCondition();
        executeUpdate(upit);
    }

    private Long executeUpdate(String query) throws SQLException {
        System.out.println(query);
        try (Statement st = DbConnection.getInstance().getConnection()
                .prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            int rowcount = st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            if (rowcount <= 0) {
                throw new RuntimeException("Update query not executed");
            }
            try (ResultSet rs = st.getGeneratedKeys()) {
                rs.next();
                try {
                    return rs.getLong(1);
                } catch (SQLException e) {
                    return -1L;
                }
            }
        }
    }

    @Override
    public List<DomainEntity> findRecordWithLimit(DomainEntity entity, int limit) throws SQLException {
        // SELECT username, topScore from player order by topScore desc limit 10;
        List<DomainEntity> objects = new LinkedList<>();
        String query = "SELECT " + entity.getShortAttributeNames() + " FROM " + entity.getTableName() + " ORDER BY " + entity.getAttributeNameForSorting() + " DESC LIMIT " + limit;
        System.out.println(query);
        PreparedStatement statement = DbConnection.getInstance().getConnection().prepareStatement(query);
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            objects.add(entity.getNewRecordForShortAttributes(rs));
        }
        return objects;
    }

//select p.username, g.score, g.date from player p join game g on p.id = g.playerId 
    public List<ScorePerGames> findScorePerGames(DomainEntity entity) throws SQLException {
        List<ScorePerGames> spgs = new LinkedList<>();
        String query = "SELECT p.username, g.score, g.date FROM player p JOIN game g ON p.id = g.playerId ORDER BY score DESC";
        System.out.println(query);
        PreparedStatement statement = DbConnection.getInstance().getConnection().prepareStatement(query);
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            ScorePerGames spg = new ScorePerGames();
            spg.setUsername(rs.getString("p.username"));
            spg.setScore(rs.getLong("g.score"));
            spg.setDate(new Date(rs.getDate("g.date").getTime()));

            spgs.add(spg);
        }
        return spgs;
    }
    
}
