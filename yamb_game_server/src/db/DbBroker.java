/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package db;

import domain.DomainEntity;
import java.util.List;
import java.util.Optional;
import java.sql.SQLException;

/**
 *
 * @author anjak
 */
public interface DbBroker {
    List<DomainEntity> getAllRecord(DomainEntity entity) throws SQLException;

    Optional<DomainEntity> findRecord(DomainEntity entity) throws SQLException;

    List<DomainEntity> findRecords(DomainEntity entity, DomainEntity parent) throws SQLException;

    Long insertRecord(DomainEntity entity) throws SQLException;

    void deleteRecord(DomainEntity entity) throws SQLException;

    void updateRecord(DomainEntity entity, DomainEntity entityld) throws Exception;

    void updateRecord(DomainEntity entity) throws SQLException;

    List<DomainEntity> findRecordWithLimit(DomainEntity entity, int limit) throws SQLException;
}
