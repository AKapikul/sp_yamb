/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import settings.SettingsLoader;

/**
 *
 * @author anjak
 */
public class DbConnection {
    
    private final Connection connection;
    private static DbConnection instance;

    private DbConnection() throws SQLException, ClassNotFoundException {
        String dbUrl = SettingsLoader.getInstance().getProperty("db.url");
        String dbUser = SettingsLoader.getInstance().getProperty("db.user");
        String dbPassword = SettingsLoader.getInstance().getProperty("db.password");
        connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
        connection.setAutoCommit(false);
    }

    public static DbConnection getInstance() {
        try {
            if (instance == null) {
                instance = new DbConnection();
            }
            return instance;
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void startTransaction() throws SQLException {
        connection.setAutoCommit(false);
    }

    public void commitTransaction() throws SQLException {
        connection.commit();
    }

    public void rollbackTransaction() throws SQLException {
        connection.rollback();
    }
}
