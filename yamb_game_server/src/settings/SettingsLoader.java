/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package settings;

import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author anjak
 */
public class SettingsLoader {
    private static SettingsLoader instance;
    private Properties databaseProperties;

    private SettingsLoader() throws IOException {
        loadProperties();
    }

    public synchronized static SettingsLoader getInstance() {
        try {
            if (instance == null) {
                instance = new SettingsLoader();
            }
            return instance;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void loadProperties() throws IOException {
        databaseProperties = new Properties();
        databaseProperties.load(SettingsLoader.class.getResourceAsStream("/resources/yamb.properties"));
    }

    public String getProperty(String key) {
        return databaseProperties.getProperty(key, "n/a");
    }
}
