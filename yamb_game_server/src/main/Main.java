/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main;

import communication.CommunicationSocket;

/**
 *
 * @author anjak
 */
public class Main {
     public static void main(String[] args) {
        new Thread(() -> {
            try {
                new CommunicationSocket().startServer();
            } catch (Exception e) {
                System.out.println(e);
            }
        }).start();
    }
}
