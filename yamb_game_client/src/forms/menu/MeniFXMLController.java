/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms.menu;

import communication.YambController;
import domain.Game;
import forms.FormSwitcher;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

/**
 *
 * @author anjak
 */
public class MeniFXMLController implements Initializable {

    private Game game;
    private boolean gameStarted = false;

    @FXML
    private MenuItem startGame;

    @FXML
    private MenuItem exitGame;

    @FXML
    private AnchorPane root;

    @FXML
    private StackPane stackPane;

    @FXML
    private void startGame(ActionEvent event) {
        try {
            if (YambController.getInstance().game != null) {
                YambController.getInstance().endGame();
            }

            Alert a = new Alert(Alert.AlertType.INFORMATION);
            a.setTitle("Informacija");
            a.setHeaderText("");
            a.setContentText("Sistem je pokrenuo igru");
            a.show();

            URL url = getClass().getResource("/forms/game/GameFXML.fxml");
            Scene scene = new Scene(FXMLLoader.load(url));
            stackPane.getChildren().add(scene.getRoot());
        } catch (Exception ex) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Greska");
            a.setHeaderText("");
            a.setContentText("Sistem ne moze da zapocne igru");
            a.show();
            System.out.println(ex);
        }
    }

    @FXML
    private void exitGame(ActionEvent event) {
        if (YambController.getInstance().game != null) {
            try {
                Alert a = new Alert(Alert.AlertType.INFORMATION);
                a.setTitle("Informacija");
                a.setHeaderText("");
                a.setContentText("Sistem je zavrsio igru");
                a.show();
                YambController.getInstance().endGame();
                YambController.getInstance().exitApp();
            } catch (Exception ex) {
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setTitle("Greska");
                a.setHeaderText("");
                a.setContentText("Sistem ne moze da zavrsi igru");
                a.show();
                System.out.println(ex);
            }
        }

        FormSwitcher.getInstance().getStage().close();
    }

    @FXML
    private void statsPerPlayer(ActionEvent event) {
        try {
            URL url = getClass().getResource("/forms/topScorePlayers/TopScorePlayersFXML.fxml");
            Scene scene = new Scene(FXMLLoader.load(url));
            stackPane.getChildren().add(scene.getRoot());
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void statsPerGame(ActionEvent event) {
        try {
            URL url = getClass().getResource("/forms/scorePerGames/ScorePerGamesFXML.fxml");
            Scene scene = new Scene(FXMLLoader.load(url));
            stackPane.getChildren().add(scene.getRoot());
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

}
