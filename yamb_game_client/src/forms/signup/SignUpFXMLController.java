/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms.signup;

import communication.YambController;
import domain.Player;
import forms.FormSwitcher;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author anjak
 */
public class SignUpFXMLController implements Initializable {

    @FXML
    private TextField txtFirstname;

    @FXML
    private TextField txtLastname;

    @FXML
    private TextField txtUsername;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private Button btnLogin;

    @FXML
    private Button btnSingup;

    @FXML
    private Label lblFirstname;

    @FXML
    private Label lblLastname;

    @FXML
    private Label lblUsername;

    @FXML
    private Label lblPassword;

    @FXML
    private Label lblError;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        handleSignup();
        handleLogin();
    }

    private void handleSignup() {
        btnSingup.setOnMouseClicked((event) -> {
            String firstname = txtFirstname.getText();
            String lastname = txtLastname.getText();
            String username = txtUsername.getText();
            String password = txtPassword.getText();
            System.out.println(firstname + lastname + username + password);
            try {
                Player player = YambController.getInstance().signup(firstname, lastname, username, password);
                System.out.println(player);
                Alert a = new Alert(Alert.AlertType.INFORMATION);
                a.setTitle("Informacija");
                a.setHeaderText("");
                a.setContentText("Uspesno ste registrovani");
                a.show();
                FormSwitcher.getInstance().setScene("/forms/login/LoginFXML.fxml");
            } catch (Exception ex) {
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setTitle("Greska");
                a.setHeaderText("");
                a.setContentText("Sistem ne moze da registruje igraca");
                a.show();
                System.out.println(ex);
                lblError.setText(ex.getMessage());
            }
        });
    }

    private void handleLogin() {
        btnLogin.setOnMouseClicked((event) -> {
            FormSwitcher.getInstance().setScene("/forms/login/LoginFXML.fxml");
        });
    }

}
