/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms.scorePerGames;

import communication.YambController;
import domain.Game;
import domain.ScorePerGames;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author anjak
 */
public class ScorePerGamesFXMLController implements Initializable {

    @FXML
    private TableView<ScorePerGames> table;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            final ObservableList<ScorePerGames> data = FXCollections.observableArrayList(
                    YambController.getInstance().getScorePerGames(new ScorePerGames())
            );

            TableColumn usernameCol = new TableColumn("Korisnicko ime");
            usernameCol.setCellValueFactory(new PropertyValueFactory("username"));
            usernameCol.prefWidthProperty().bind(table.widthProperty().divide(3));
            TableColumn scoreCol = new TableColumn("Rezultat");
            scoreCol.setCellValueFactory(new PropertyValueFactory("score"));
            scoreCol.prefWidthProperty().bind(table.widthProperty().divide(3));
            TableColumn dateCol = new TableColumn("Datum igre");
            dateCol.setCellValueFactory(new PropertyValueFactory("date"));
            dateCol.prefWidthProperty().bind(table.widthProperty().divide(3));
            dateCol.setCellFactory(column -> {
                TableCell<ScorePerGames, Date> cell = new TableCell<ScorePerGames, Date>() {
                    private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

                    @Override
                    protected void updateItem(Date item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                        } else {
                            setText(format.format(item));
                        }
                    }
                };

                return cell;
            });
            ObservableList<String> list = FXCollections.observableArrayList();
            table.setItems(data);
            table.getColumns().addAll(usernameCol, scoreCol, dateCol);
            Alert a = new Alert(Alert.AlertType.INFORMATION);
            a.setTitle("Informacija");
            a.setHeaderText("");
            a.setContentText("Sistem je nasao rezultate igraca");
            a.show();
        } catch (Exception ex) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Greska");
            a.setHeaderText("");
            a.setContentText("Sistem ne moze da nadje rezultate igraca");
            a.show();
            System.out.println(ex);
        }
    }

}
