/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms.login;

import forms.FormSwitcher;
import communication.YambController;
import domain.Player;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import utils.PlayerSession;

/**
 * FXML Controller class
 *
 * @author anjak
 */
public class LoginFXMLController implements Initializable {

    @FXML
    private TextField txtUsername;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private Button btnLogin;

    @FXML
    private Button btnSignup;

    @FXML
    private Label lblUsername;

    @FXML
    private Label lblPassword;

    @FXML
    private Label lblError;

    @FXML
    private Label lblForgotPassword;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        handleLogin();
        handleSignup();
    }

    private void handleLogin() {
        btnLogin.setOnMouseClicked((event) -> {
            String username = txtUsername.getText();
            String password = txtPassword.getText();
            try {
                Player player = YambController.getInstance().logIn(username, password);
                Alert a = new Alert(Alert.AlertType.INFORMATION);
                a.setTitle("Informacija");
                a.setHeaderText("");
                a.setContentText("Uspesno ste prijavljeni");
                a.show();
                PlayerSession.getInstance().setPlayer(player);
                FormSwitcher.getInstance().setScene("/forms/menu/Meni.fxml");
            } catch (Exception ex) {
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setTitle("Greska");
                a.setHeaderText("");
                a.setContentText(ex.getMessage());
                a.show();
                System.out.println("Error:" + ex);
                lblError.setText(ex.getMessage());
            }
        });
    }

    public void handleForgotPassword() {
        lblForgotPassword.setOnMouseClicked((event) -> {
            FormSwitcher.getInstance().setScene("/forms/passwordChange/PasswordChangeFXML.fxml");
        });
    }

    private void handleSignup() {
        btnSignup.setOnMouseClicked((event) -> {
            FormSwitcher.getInstance().setScene("/forms/signup/SignUpFXML.fxml");
        });
    }

}
