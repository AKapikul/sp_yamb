/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms.topScorePlayers;

import communication.YambController;
import domain.Player;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author anjak
 */
public class TopScorePlayersFXMLController implements Initializable {

    @FXML
    private TableView<Player> table;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            final ObservableList<Player> data = FXCollections.observableArrayList(
                    YambController.getInstance().getTop10Players(new Player())
            );
            TableColumn usernameCol = new TableColumn("Korisnicko ime");
            usernameCol.setCellValueFactory(new PropertyValueFactory("username"));
            usernameCol.prefWidthProperty().bind(table.widthProperty().divide(2));
            TableColumn topScoreCol = new TableColumn("Rezultat");
            topScoreCol.setCellValueFactory(new PropertyValueFactory("topScore"));
            topScoreCol.prefWidthProperty().bind(table.widthProperty().divide(2));
            ObservableList<String> list = FXCollections.observableArrayList();
            table.setItems(data);
            table.getColumns().addAll(usernameCol, topScoreCol);
            Alert a = new Alert(Alert.AlertType.INFORMATION);
            a.setTitle("Informacija");
            a.setHeaderText("");
            a.setContentText("Sistem je nasao rezultate");
            a.show();
        } catch (Exception ex) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Greska");
            a.setHeaderText("");
            a.setContentText("Sistem ne moze da nadje rezultate");
            a.show();
            System.out.println(ex);
        }

    }

}
