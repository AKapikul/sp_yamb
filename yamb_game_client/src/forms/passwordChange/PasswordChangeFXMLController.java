/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms.passwordChange;

import communication.YambController;
import domain.Player;
import forms.FormSwitcher;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author anjak
 */
public class PasswordChangeFXMLController implements Initializable {

    @FXML
    private TextField txtUsername;
    @FXML
    private TextField txtNewPassword;

    @FXML
    private Button btnConfirm;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        handlePasswordChange();
    }

    private void handlePasswordChange() {
        btnConfirm.setOnMouseClicked((event) -> {
            try {
                String username = txtUsername.getText();
                String newPassword = txtNewPassword.getText();
                Player p = new Player(username, newPassword);
                YambController.getInstance().update(p);
                Alert a = new Alert(Alert.AlertType.INFORMATION);
                a.setTitle("Informacija");
                a.setHeaderText("");
                a.setContentText("Sistem je uspesno izmenio igraca");
                a.show();
                FormSwitcher.getInstance().setScene("/forms/login/LoginFXML.fxml");
            } catch (Exception ex) {
                System.out.println(ex);
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setTitle("Greska");
                a.setHeaderText("");
                a.setContentText("Sistem ne moze da izmeni igraca");
                a.show();
            }
        });
    }

}
