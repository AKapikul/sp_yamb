/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms.game;

import communication.YambController;
import domain.Player;
import domain.ResultEnum;
import javafx.animation.*;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.event.Event;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import utils.PlayerSession;
import utils.Roller;

/**
 * FXML Controller class
 *
 * @author anjak
 */
public class GameFXMLController implements Initializable {

    // Label na poljima
    @FXML
    public Label lblUpOnes;
    @FXML
    public Label lblDownOnes;
    @FXML
    public Label lblFreeOnes;
    @FXML
    public Label lblHandOnes;

    @FXML
    public Label lblUpTwos;
    @FXML
    public Label lblDownTwos;
    @FXML
    public Label lblFreeTwos;
    @FXML
    public Label lblHandTwos;

    @FXML
    public Label lblUpThrees;
    @FXML
    public Label lblDownThrees;
    @FXML
    public Label lblFreeThrees;
    @FXML
    public Label lblHandThrees;

    @FXML
    public Label lblUpFours;
    @FXML
    public Label lblDownFours;
    @FXML
    public Label lblFreeFours;
    @FXML
    public Label lblHandFours;

    @FXML
    public Label lblUpFives;
    @FXML
    public Label lblDownFives;
    @FXML
    public Label lblFreeFives;
    @FXML
    public Label lblHandFives;

    @FXML
    public Label lblUpSixes;
    @FXML
    public Label lblDownSixes;
    @FXML
    public Label lblFreeSixes;
    @FXML
    public Label lblHandSixes;

    @FXML
    public Label lblUpSumOneToSix;
    @FXML
    public Label lblDownSumOneToSix;
    @FXML
    public Label lblFreeSumOneToSix;
    @FXML
    public Label lblHandSumOneToSix;

    @FXML
    public Label lblUpMax;
    @FXML
    public Label lblDownMax;
    @FXML
    public Label lblFreeMax;
    @FXML
    public Label lblHandMax;

    @FXML
    public Label lblUpMin;
    @FXML
    public Label lblDownMin;
    @FXML
    public Label lblFreeMin;
    @FXML
    public Label lblHandMin;

    @FXML
    public Label lblUpSubstractMaxMinMultipleOnes;
    @FXML
    public Label lblDownSubstractMaxMinMultipleOnes;
    @FXML
    public Label lblFreeSubstractMaxMinMultipleOnes;
    @FXML
    public Label lblHandSubstractMaxMinMultipleOnes;

    @FXML
    public Label lblUpKenta;
    @FXML
    public Label lblDownKenta;
    @FXML
    public Label lblFreeKenta;
    @FXML
    public Label lblHandKenta;

    @FXML
    public Label lblUpTriling;
    @FXML
    public Label lblDownTriling;
    @FXML
    public Label lblFreeTriling;
    @FXML
    public Label lblHandTriling;

    @FXML
    public Label lblUpFul;
    @FXML
    public Label lblDownFul;
    @FXML
    public Label lblFreeFul;
    @FXML
    public Label lblHandFul;

    @FXML
    public Label lblUpPoker;
    @FXML
    public Label lblDownPoker;
    @FXML
    public Label lblFreePoker;
    @FXML
    public Label lblHandPoker;

    @FXML
    public Label lblUpYamb;
    @FXML
    public Label lblDownYamb;
    @FXML
    public Label lblFreeYamb;
    @FXML
    public Label lblHandYamb;

    @FXML
    public Label lblUpSumKentaToYamb;
    @FXML
    public Label lblDownSumKentaToYamb;
    @FXML
    public Label lblFreeSumKentaToYamb;
    @FXML
    public Label lblHandSumKentaToYamb;

    @FXML
    public Label lblAllSumOneToSix;
    @FXML
    public Label lblAllSumMaxMin;
    @FXML
    public Label lblAllSumKentaToYamb;

    @FXML
    public Label lblTotalSum;

    @FXML
    public Label lblTopScore;

    // Polja 
    @FXML
    public Rectangle rectangleUpOnes;
    @FXML
    public Rectangle rectangleDownOnes;
    @FXML
    public Rectangle rectangleFreeOnes;
    @FXML
    public Rectangle rectangleHandOnes;

    @FXML
    public Rectangle rectangleUpTwos;
    @FXML
    public Rectangle rectangleDownTwos;
    @FXML
    public Rectangle rectangleFreeTwos;
    @FXML
    public Rectangle rectangleHandTwos;

    @FXML
    public Rectangle rectangleUpThrees;
    @FXML
    public Rectangle rectangleDownThrees;
    @FXML
    public Rectangle rectangleFreeThrees;
    @FXML
    public Rectangle rectangleHandThrees;

    @FXML
    public Rectangle rectangleUpFours;
    @FXML
    public Rectangle rectangleDownFours;
    @FXML
    public Rectangle rectangleFreeFours;
    @FXML
    public Rectangle rectangleHandFours;

    @FXML
    public Rectangle rectangleUpFives;
    @FXML
    public Rectangle rectangleDownFives;
    @FXML
    public Rectangle rectangleFreeFives;
    @FXML
    public Rectangle rectangleHandFives;

    @FXML
    public Rectangle rectangleUpSixes;
    @FXML
    public Rectangle rectangleDownSixes;
    @FXML
    public Rectangle rectangleFreeSixes;
    @FXML
    public Rectangle rectangleHandSixes;

    @FXML
    public Rectangle rectangleUpSumOneToSix;
    @FXML
    public Rectangle rectangleDownSumOneToSix;
    @FXML
    public Rectangle rectangleFreeSumOneToSix;
    @FXML
    public Rectangle rectangleHandSumOneToSix;

    @FXML
    public Rectangle rectangleUpMax;
    @FXML
    public Rectangle rectangleDownMax;
    @FXML
    public Rectangle rectangleFreeMax;
    @FXML
    public Rectangle rectangleHandMax;

    @FXML
    public Rectangle rectangleUpMin;
    @FXML
    public Rectangle rectangleDownMin;
    @FXML
    public Rectangle rectangleFreeMin;
    @FXML
    public Rectangle rectangleHandMin;

    @FXML
    public Rectangle rectangleUpSubstractMaxMinMultipleOnes;
    @FXML
    public Rectangle rectangleDownSubstractMaxMinMultipleOnes;
    @FXML
    public Rectangle rectangleFreeSubstractMaxMinMultipleOnes;
    @FXML
    public Rectangle rectangleHandSubstractMaxMinMultipleOnes;

    @FXML
    public Rectangle rectangleUpKenta;
    @FXML
    public Rectangle rectangleDownKenta;
    @FXML
    public Rectangle rectangleFreeKenta;
    @FXML
    public Rectangle rectangleHandKenta;

    @FXML
    public Rectangle rectangleUpTriling;
    @FXML
    public Rectangle rectangleDownTriling;
    @FXML
    public Rectangle rectangleFreeTriling;
    @FXML
    public Rectangle rectangleHandTriling;

    @FXML
    public Rectangle rectangleUpFul;
    @FXML
    public Rectangle rectangleDownFul;
    @FXML
    public Rectangle rectangleFreeFul;
    @FXML
    public Rectangle rectangleHandFul;

    @FXML
    public Rectangle rectangleUpPoker;
    @FXML
    public Rectangle rectangleDownPoker;
    @FXML
    public Rectangle rectangleFreePoker;
    @FXML
    public Rectangle rectangleHandPoker;

    @FXML
    public Rectangle rectangleUpYamb;
    @FXML
    public Rectangle rectangleDownYamb;
    @FXML
    public Rectangle rectangleFreeYamb;
    @FXML
    public Rectangle rectangleHandYamb;

    @FXML
    public Rectangle rectangleUpSumKentaToYamb;
    @FXML
    public Rectangle rectangleDownSumKentaToYamb;
    @FXML
    public Rectangle rectangleFreeSumKentaToYamb;
    @FXML
    public Rectangle rectangleHandSumKentaToYamb;

    @FXML
    public Rectangle rectangleAllSumOneToSix;
    @FXML
    public Rectangle rectangleAllSumMaxMin;
    @FXML
    public Rectangle rectangleAllSumKentaToYamb;

    @FXML
    public Rectangle rectangleTotalSum;

    @FXML
    public Label remainingRolls;

    @FXML
    public ImageView rollButton;
    @FXML
    public Image rollButtonImage = new Image("/images/play.png");
    @FXML
    public Image rollButtonHoverImage = new Image("/images/play_hov.png");
    @FXML
    public Image rollButtonDisableImage = new Image("/images/play_disable.png");

    @FXML
    public Image imageOne = new Image("/images/dice1.png");
    @FXML
    public Image imageTwo = new Image("/images/dice2.png");
    @FXML
    public Image imageThree = new Image("/images/dice3.png");
    @FXML
    public Image imageFour = new Image("/images/dice4.png");
    @FXML
    public Image imageFive = new Image("/images/dice5.png");
    @FXML
    public Image imageSix = new Image("/images/dice6.png");
    @FXML
    public ImageView diceZero;
    @FXML
    public ImageView diceOne;
    @FXML
    public ImageView diceTwo;
    @FXML
    public ImageView diceThree;
    @FXML
    public ImageView diceFour;
    @FXML
    private ImageView clickedDiceZero;
    @FXML
    private ImageView clickedDiceOne;
    @FXML
    private ImageView clickedDiceTwo;
    @FXML
    private ImageView clickedDiceThree;
    @FXML
    private ImageView clickedDiceFour;

    @FXML
    private Pane upPane;
    @FXML
    private Pane downPane;
    @FXML
    private Pane freePane;
    @FXML
    private Pane handPane;
    @FXML
    private Rectangle rollsRectangle;

    public Player player;

    public Map<String, Map<String, Boolean>> selectedFields = new HashMap<>();
    private final List<Rectangle> pointsRectangle = new ArrayList<>();
    private final List<Label> pointsLabels = new ArrayList<>();

    private boolean popunioSveOsimRucne = false;

    @FXML
    public void initialize(URL url, ResourceBundle rb) {
        init();
    }

    private void init() {
        player = PlayerSession.getInstance().getPlayer();
        lblTopScore.setText(player.getTopScore() + "");

        Map<String, Boolean> initialValuesMapUp = new HashMap<>();
        Map<String, Boolean> initialValuesMapDown = new HashMap<>();
        Map<String, Boolean> initialValuesMapFree = new HashMap<>();
        Map<String, Boolean> initialValuesMapHande = new HashMap<>();

        Arrays.asList(ResultEnum.values()).forEach(value -> {
            initialValuesMapUp.put(value.toString(), false);
            initialValuesMapDown.put(value.toString(), false);
            initialValuesMapFree.put(value.toString(), false);
            initialValuesMapHande.put(value.toString(), false);
        });

        selectedFields.put("upPane", initialValuesMapUp);
        selectedFields.put("downPane", initialValuesMapDown);
        selectedFields.put("freePane", initialValuesMapFree);
        selectedFields.put("handPane", initialValuesMapHande);

        lblAllSumOneToSix.setText(0 + "");
        lblAllSumMaxMin.setText(0 + "");
        lblAllSumKentaToYamb.setText(0 + "");
        lblTotalSum.setText(0 + "");

        DropShadow ds = new DropShadow();
        ds.setOffsetY(2.0);
        ds.setOffsetX(2.0);
        ds.setColor(Color.GRAY);

        resetRollsRectanglesDices();

        rollButton.setEffect(ds);

        upPane.getChildren().forEach(child -> {
            if (child.getClass() == VBox.class) {
                ((VBox) child).getChildren().forEach(r -> {
                    if (r.getClass() == Rectangle.class) {
                        pointsRectangle.add((Rectangle) r);
                    }
                });
            } else if (child.getClass() == Label.class) {
                pointsLabels.add((Label) child);
            }
        });

        downPane.getChildren().forEach(child -> {
            if (child.getClass() == VBox.class) {
                ((VBox) child).getChildren().forEach(r -> {
                    if (r.getClass() == Rectangle.class) {
                        pointsRectangle.add((Rectangle) r);
                    }
                });
            } else if (child.getClass() == Label.class) {
                pointsLabels.add((Label) child);
            }
        });

        freePane.getChildren().forEach(child -> {
            if (child.getClass() == VBox.class) {
                ((VBox) child).getChildren().forEach(r -> {
                    if (r.getClass() == Rectangle.class) {
                        pointsRectangle.add((Rectangle) r);
                    }
                });
            } else if (child.getClass() == Label.class) {
                pointsLabels.add((Label) child);
            }
        });

        handPane.getChildren().forEach(child -> {
            if (child.getClass() == VBox.class) {
                ((VBox) child).getChildren().forEach(r -> {
                    if (r.getClass() == Rectangle.class) {
                        pointsRectangle.add((Rectangle) r);
                    }
                });
            } else if (child.getClass() == Label.class) {
                pointsLabels.add((Label) child);
            }
        });

        lblAllSumOneToSix.setText(0 + "");
        lblAllSumMaxMin.setText(0 + "");
        lblAllSumKentaToYamb.setText(0 + "");

        resetEverything();
    }

    public void Roll() {
        try {
            if (PlayerSession.getInstance().getCounter() == 0 && remainingRolls() == 2) {
                System.out.println("Pocetak");
                YambController.getInstance().startGame();
            }
            setRemainingRolls();
            Roller.rollDices();
            showPlayerResults();
            if (Roller.getRollNum() < 3) {
                startRollingDices();
            } else if (Roller.getRollNum() == 3) {
                disableRollButon();
                startRollingDices();
            } else {
                rollButton.setDisable(true);
            }
        } catch (Exception ex) {
            Logger.getLogger(GameFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean checkDownPane(String key, ResultEnum value) {
        List<ResultEnum> yambValues = Arrays.asList(ResultEnum.values());
        int index = yambValues.indexOf(value);

        if (index == 0) {
            return true;
        }

        return selectedFields.get(key).get(yambValues.get(index - 1).toString());
    }

    private boolean checkUpPane(String key, ResultEnum value) {
        List<ResultEnum> yambValues = Arrays.asList(ResultEnum.values());
        int index = yambValues.indexOf(value);

        if (index == yambValues.size() - 1) {
            return true;
        }

        return selectedFields.get(key).get(yambValues.get(index + 1).toString());

    }

    private boolean canWriteResult(String key, ResultEnum value) {
        switch (key) {
            case "downPane":
                return checkDownPane(key, value);
            case "upPane":
                return checkUpPane(key, value);
            case "handPane":
                return remainingRolls() == 1;
            default:
                return true;
        }
    }

    public void calculateGameResults() {
        selectedFields.keySet().forEach(key -> {
            Map<String, Boolean> selections = selectedFields.get(key);

            if ((!selections.get(ResultEnum.ONE.toString()))) {
                Label l = findLabel(key, ResultEnum.ONE.toString());
                if (canWriteResult(key, ResultEnum.ONE) || popunioSveOsimRucne) {
                    l.setText(player.calculateSimpleNumbers(Roller.dice, 1) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.TWO.toString()))) {
                Label l = findLabel(key, ResultEnum.TWO.toString());
                if (canWriteResult(key, ResultEnum.TWO) || popunioSveOsimRucne) {
                    l.setText(player.calculateSimpleNumbers(Roller.dice, 2) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.THREE.toString()))) {
                Label l = findLabel(key, ResultEnum.THREE.toString());
                if (canWriteResult(key, ResultEnum.THREE) || popunioSveOsimRucne) {
                    l.setText(player.calculateSimpleNumbers(Roller.dice, 3) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.FOUR.toString()))) {
                Label l = findLabel(key, ResultEnum.FOUR.toString());
                if (canWriteResult(key, ResultEnum.FOUR) || popunioSveOsimRucne) {
                    l.setText(player.calculateSimpleNumbers(Roller.dice, 4) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.FIVE.toString()))) {
                Label l = findLabel(key, ResultEnum.FIVE.toString());
                if (canWriteResult(key, ResultEnum.FIVE) || popunioSveOsimRucne) {
                    l.setText(player.calculateSimpleNumbers(Roller.dice, 5) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.SIX.toString()))) {
                Label l = findLabel(key, ResultEnum.SIX.toString());
                if (canWriteResult(key, ResultEnum.SIX) || popunioSveOsimRucne) {
                    l.setText(player.calculateSimpleNumbers(Roller.dice, 6) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.MAX.toString()))) {
                Label l = findLabel(key, ResultEnum.MAX.toString());
                if (canWriteResult(key, ResultEnum.MAX) || popunioSveOsimRucne) {
                    l.setText(player.calculateMax(Roller.dice) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.MIN.toString()))) {
                Label l = findLabel(key, ResultEnum.MIN.toString());
                if (canWriteResult(key, ResultEnum.MIN) || popunioSveOsimRucne) {
                    l.setText(player.calculateMin(Roller.dice) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.KENTA.toString()))) {
                Label l = findLabel(key, ResultEnum.KENTA.toString());
                if (canWriteResult(key, ResultEnum.KENTA) || popunioSveOsimRucne) {
                    l.setText(player.calculateKenta(Roller.dice, remainingRolls() + 1) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.TRILING.toString()))) {
                Label l = findLabel(key, ResultEnum.TRILING.toString());
                if (canWriteResult(key, ResultEnum.TRILING) || popunioSveOsimRucne) {
                    l.setText(player.calculateTriling(Roller.dice) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.FUL.toString()))) {
                Label l = findLabel(key, ResultEnum.FUL.toString());
                if (canWriteResult(key, ResultEnum.FUL) || popunioSveOsimRucne) {
                    l.setText(player.calculateFul(Roller.dice) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.POKER.toString()))) {
                Label l = findLabel(key, ResultEnum.POKER.toString());
                if (canWriteResult(key, ResultEnum.POKER) || popunioSveOsimRucne) {
                    l.setText(player.calculatePoker(Roller.dice) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
            if ((!selections.get(ResultEnum.YAMB.toString()))) {
                Label l = findLabel(key, ResultEnum.YAMB.toString());
                if (canWriteResult(key, ResultEnum.YAMB) || popunioSveOsimRucne) {
                    l.setText(player.calculateYamb(Roller.dice) + "");
                    l.disableProperty().set(false);
                } else {
                    l.setText(0 + "");
                    l.disableProperty().set(true);
                }
            }
        });
    }

    private Rectangle findRectangle(String paneId, String result) {
        List<Rectangle> rectangles = pointsRectangle.stream().filter(r -> {
            return ((Rectangle) r).getId().equals(result) && ((Rectangle) r).getParent().getParent().getId().equals(paneId);
        }).collect(Collectors.toList());

        return !rectangles.isEmpty() ? rectangles.get(0) : null;
    }

    private Label findLabel(String paneId, String result) {
        List<Label> labels = pointsLabels.stream().filter(l -> {
            return ((Label) l).getId().equals(result) && ((Label) l).getParent().getId().equals(paneId);
        }).collect(Collectors.toList());

        return !labels.isEmpty() ? labels.get(0) : null;
    }

    public void setRectangles() {
        selectedFields.keySet().forEach(key -> {
            Map<String, Boolean> selections = selectedFields.get(key);

            if ((!selections.get(ResultEnum.ONE.toString()) && (player.calculateSimpleNumbers(Roller.dice, 1) != 0))) {
                if (canWriteResult(key, ResultEnum.ONE) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.ONE.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.ONE.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.ONE.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.TWO.toString()) && (player.calculateSimpleNumbers(Roller.dice, 2) != 0))) {
                if (canWriteResult(key, ResultEnum.TWO) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.TWO.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.TWO.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.TWO.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.THREE.toString()) && (player.calculateSimpleNumbers(Roller.dice, 3) != 0))) {
                if (canWriteResult(key, ResultEnum.THREE) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.THREE.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.THREE.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.THREE.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.FOUR.toString()) && (player.calculateSimpleNumbers(Roller.dice, 4) != 0))) {
                if (canWriteResult(key, ResultEnum.FOUR) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.FOUR.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.FOUR.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.FOUR.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.FIVE.toString()) && (player.calculateSimpleNumbers(Roller.dice, 5) != 0))) {
                if (canWriteResult(key, ResultEnum.FIVE) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.FIVE.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.FIVE.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.FIVE.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.SIX.toString()) && (player.calculateSimpleNumbers(Roller.dice, 6) != 0))) {
                if (canWriteResult(key, ResultEnum.SIX) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.SIX.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.SIX.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.SIX.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.MAX.toString()) && (player.calculateMax(Roller.dice) != 0))) {
                if (canWriteResult(key, ResultEnum.MAX) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.MAX.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.MAX.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.MAX.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.MIN.toString()) && (player.calculateMin(Roller.dice) != 0))) {
                if (canWriteResult(key, ResultEnum.MIN) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.MIN.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.MIN.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.MIN.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.KENTA.toString()) && (player.calculateKenta(Roller.dice, remainingRolls() + 1) != 0))) {
                if (canWriteResult(key, ResultEnum.KENTA) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.KENTA.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.KENTA.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.KENTA.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.TRILING.toString()) && (player.calculateTriling(Roller.dice) != 0))) {
                if (canWriteResult(key, ResultEnum.TRILING) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.TRILING.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.TRILING.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.TRILING.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.FUL.toString()) && (player.calculateFul(Roller.dice) != 0))) {
                if (canWriteResult(key, ResultEnum.FUL) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.FUL.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.FUL.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.FUL.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.POKER.toString()) && (player.calculatePoker(Roller.dice) != 0))) {
                if (canWriteResult(key, ResultEnum.POKER) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.POKER.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.POKER.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.POKER.toString()).setOpacity(0);
            }
            if ((!selections.get(ResultEnum.YAMB.toString()) && (player.calculateYamb(Roller.dice) != 0))) {
                if (canWriteResult(key, ResultEnum.YAMB) || popunioSveOsimRucne) {
                    findRectangle(key, ResultEnum.YAMB.toString()).setOpacity(1);
                } else {
                    findRectangle(key, ResultEnum.YAMB.toString()).setOpacity(0);
                }
            } else {
                findRectangle(key, ResultEnum.YAMB.toString()).setOpacity(0);
            }
        });
    }

    /**
     * Reset Actions
     *
     */
    public void resetScoreSheet() {
        String defaultValueScores = "0";

        selectedFields.keySet().forEach(key -> {
            Map<String, Boolean> selections = selectedFields.get(key);

            Arrays.asList(ResultEnum.values()).forEach(value -> {
                if (!selections.get(value.toString())) {
                    findLabel(key, value.toString()).setText("0");
                }
            });
        });
    }

    public void resetRollsRectanglesDices() {
        remainingRolls.setText("3");
        enableRollButon();
        resetRectangles();
        resetSelectedDices();
        resetDiceImages();
        Roller.resetRollNum();
    }

    public void resetEverything() {
        remainingRolls.setText("3");
        enableRollButon();
        resetRectangles();
        resetLabels();
        resetSelectedDices();
        Roller.resetRollNum();
        resetDiceImages();
    }

    public void resetDiceImages() {
        diceZero.setEffect(null);
        diceOne.setEffect(null);
        diceTwo.setEffect(null);
        diceThree.setEffect(null);
        diceFour.setEffect(null);

        diceZero.setImage(null);
        diceOne.setImage(null);
        diceTwo.setImage(null);
        diceThree.setImage(null);
        diceFour.setImage(null);
    }

    public void resetSelectedDices() {
        for (int i = 0; i < Roller.selectedDice.length; i++) {
            Roller.selectedDice[i] = false;
        }
        clickedDiceZero.setOpacity(0);
        clickedDiceOne.setOpacity(0);
        clickedDiceTwo.setOpacity(0);
        clickedDiceThree.setOpacity(0);
        clickedDiceFour.setOpacity(0);
    }

    public void resetRectangles() {
        pointsRectangle.forEach(rectangle -> {
            rectangle.setOpacity(0);
        });
    }

    public void resetLabels() {
        pointsLabels.forEach(label -> {
            label.setText(0 + "");
            label.setDisable(true);
        });
    }

    public void showPlayerResults() {
        PauseTransition delayDisplay = new PauseTransition(Duration.seconds(0.6));
        delayDisplay.setOnFinished(event -> calculateGameResults());
        delayDisplay.play();
        PauseTransition delayRectangle1 = new PauseTransition(Duration.seconds(0.6));
        delayRectangle1.setOnFinished(event -> setRectangles());
        delayRectangle1.play();
    }

    public int remainingRolls() {
        int rolls = 2;
        int counter = 0;

        for (String pane : Arrays.asList("downPane", "freePane", "upPane")) {
            for (String field : Arrays.asList("SIMPLE_SUM", "SUB_MAX_MIN", "DOWN_SUM")) {
                if (selectedFields.get(pane).get(field) != null && selectedFields.get(pane).get(field)) {
                    counter++;
                }
            }
        }

        if (counter >= 8) {
            popunioSveOsimRucne = true;
            Roller.setMaxRollNum();
        }

        return rolls - Roller.getRollNum();
    }

    public void setRemainingRolls() {
        remainingRolls.setText(String.valueOf(remainingRolls()));
    }

    public void startRollingDices() {
        stopRollingDices();
    }

    public void stopRollingDices() {

        PauseTransition delay0 = new PauseTransition(Duration.seconds(0.5));
        delay0.setOnFinished(event -> setDice0Image());

        PauseTransition delay1 = new PauseTransition(Duration.seconds(0.53));
        delay1.setOnFinished(event -> setDice1Image());

        PauseTransition delay2 = new PauseTransition(Duration.seconds(0.56));
        delay2.setOnFinished(event -> setDice2Image());

        PauseTransition delay3 = new PauseTransition(Duration.seconds(0.59));
        delay3.setOnFinished(event -> setDice3Image());

        PauseTransition delay4 = new PauseTransition(Duration.seconds(0.62));
        delay4.setOnFinished(event -> setDice4Image());

        ParallelTransition parallelTransition = new ParallelTransition();
        parallelTransition.getChildren().addAll(
                delay0,
                delay1,
                delay2,
                delay3,
                delay4
        );
        parallelTransition.setCycleCount(1);
        parallelTransition.play();
    }

    public void enableRollButon() {
        rollButton.setDisable(false);
        rollButton.setImage(rollButtonImage);
        rollButton.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent evt) {
                rollButton.setImage(rollButtonHoverImage);
            }
        });
        rollButton.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent evt) {
                rollButton.setImage(rollButtonImage);
            }
        });
    }

    public void disableRollButon() {
        rollButton.setDisable(true);
        rollButton.setImage(rollButtonDisableImage);
        rollButton.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent evt) {
                rollButton.setImage(rollButtonDisableImage);
            }
        });
        rollButton.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent evt) {
                rollButton.setImage(rollButtonDisableImage);
            }
        });
    }

    public void setDice0Image() {
        DropShadow ds = new DropShadow();
        ds.setOffsetY(2.0);
        ds.setOffsetX(2.0);
        ds.setColor(Color.GRAY);
        diceZero.setEffect(ds);

        switch (Roller.dice[0]) {
            case 1:
                diceZero.setImage(imageOne);
                break;
            case 2:
                diceZero.setImage(imageTwo);
                break;
            case 3:
                diceZero.setImage(imageThree);
                break;
            case 4:
                diceZero.setImage(imageFour);
                break;
            case 5:
                diceZero.setImage(imageFive);
                break;
            case 6:
                diceZero.setImage(imageSix);
                break;
        }
    }

    public void setDice1Image() {
        DropShadow ds = new DropShadow();
        ds.setOffsetY(2.0);
        ds.setOffsetX(2.0);
        ds.setColor(Color.GRAY);
        diceOne.setEffect(ds);

        switch (Roller.dice[1]) {
            case 1:
                diceOne.setImage(imageOne);
                break;
            case 2:
                diceOne.setImage(imageTwo);
                break;
            case 3:
                diceOne.setImage(imageThree);
                break;
            case 4:
                diceOne.setImage(imageFour);
                break;
            case 5:
                diceOne.setImage(imageFive);
                break;
            case 6:
                diceOne.setImage(imageSix);
                break;
        }
    }

    public void setDice2Image() {
        DropShadow ds = new DropShadow();
        ds.setOffsetY(2.0);
        ds.setOffsetX(2.0);
        ds.setColor(Color.GRAY);
        diceTwo.setEffect(ds);

        switch (Roller.dice[2]) {
            case 1:
                diceTwo.setImage(imageOne);
                break;
            case 2:
                diceTwo.setImage(imageTwo);
                break;
            case 3:
                diceTwo.setImage(imageThree);
                break;
            case 4:
                diceTwo.setImage(imageFour);
                break;
            case 5:
                diceTwo.setImage(imageFive);
                break;
            case 6:
                diceTwo.setImage(imageSix);
                break;
        }
    }

    public void setDice3Image() {
        DropShadow ds = new DropShadow();
        ds.setOffsetY(2.0);
        ds.setOffsetX(2.0);
        ds.setColor(Color.GRAY);
        diceThree.setEffect(ds);

        switch (Roller.dice[3]) {
            case 1:
                diceThree.setImage(imageOne);
                break;
            case 2:
                diceThree.setImage(imageTwo);
                break;
            case 3:
                diceThree.setImage(imageThree);
                break;
            case 4:
                diceThree.setImage(imageFour);
                break;
            case 5:
                diceThree.setImage(imageFive);
                break;
            case 6:
                diceThree.setImage(imageSix);
                break;
        }
    }

    public void setDice4Image() {
        DropShadow ds = new DropShadow();
        ds.setOffsetY(2.0);
        ds.setOffsetX(2.0);
        ds.setColor(Color.GRAY);
        diceFour.setEffect(ds);

        switch (Roller.dice[4]) {
            case 1:
                diceFour.setImage(imageOne);
                break;
            case 2:
                diceFour.setImage(imageTwo);
                break;
            case 3:
                diceFour.setImage(imageThree);
                break;
            case 4:
                diceFour.setImage(imageFour);
                break;
            case 5:
                diceFour.setImage(imageFive);
                break;
            case 6:
                diceFour.setImage(imageSix);
                break;
        }
    }

    public void clickDice0() {
        if (Roller.selectedDice[0]) {
            Roller.selectedDice[0] = false;
            clickedDiceZero.setOpacity(0);
        } else {
            Roller.selectedDice[0] = true;
            clickedDiceZero.setImage(new Image("images/clicked.png"));
            clickedDiceZero.setOpacity(1);
        }
    }

    public void clickDice1() {
        if (Roller.selectedDice[1]) {
            Roller.selectedDice[1] = false;
            clickedDiceOne.setOpacity(0);
        } else {
            Roller.selectedDice[1] = true;
            clickedDiceOne.setImage(new Image("images/clicked.png"));
            clickedDiceOne.setOpacity(1);
        }
    }

    public void clickDice2() {
        if (Roller.selectedDice[2]) {
            Roller.selectedDice[2] = false;
            clickedDiceTwo.setOpacity(0);
        } else {
            Roller.selectedDice[2] = true;
            clickedDiceTwo.setImage(new Image("images/clicked.png"));
            clickedDiceTwo.setOpacity(1);
        }
    }

    public void clickDice3() {
        if (Roller.selectedDice[3]) {
            Roller.selectedDice[3] = false;
            clickedDiceThree.setOpacity(0);
        } else {
            Roller.selectedDice[3] = true;
            clickedDiceThree.setImage(new Image("images/clicked.png"));
            clickedDiceThree.setOpacity(1);
        }
    }

    public void clickDice4() {
        if (Roller.selectedDice[4]) {
            Roller.selectedDice[4] = false;
            clickedDiceFour.setOpacity(0);
        } else {
            Roller.selectedDice[4] = true;
            clickedDiceFour.setImage(new Image("images/clicked.png"));
            clickedDiceFour.setOpacity(1);
        }
    }

    private void updateMapValues(Event event, ResultEnum value) {
        String paneName = ((Label) event.getSource()).getParent().getId();
        Label label = (Label) event.getSource();
        label.setOpacity(1);
        label.setDisable(true);
        label.setTextFill(Color.LIGHTBLUE);
        label.setCursor(Cursor.DEFAULT);
        Map<String, Boolean> keyMap = selectedFields.get(paneName);
        keyMap.put(value.toString(), true);
        selectedFields.put(paneName, keyMap);
        label.setOnMouseClicked(null);
        pointsLabels.forEach(l -> {
            l.setDisable(true);
        });
        PlayerSession.getInstance().setCounter(PlayerSession.getInstance().getCounter() + 1);
        PlayerSession.getInstance().setScore(PlayerSession.getInstance().getScore() + Integer.parseInt(label.getText()));
        System.out.println(PlayerSession.getInstance().getScore());
    }

    private void calculateTotalSum() {
        calculateMaxMin();
        calculateTotalSimpleNumbers();
        calculateTotalDownSum();

        boolean competed = true;

        for (String key : selectedFields.keySet()) {
            Map<String, Boolean> selections = selectedFields.get(key);

            if (selections.get("SIMPLE_SUM") != null && !selections.get("SIMPLE_SUM")) {
                competed = false;
                break;
            }

            if (selections.get("SUB_MAX_MIN") != null && !selections.get("SUB_MAX_MIN")) {
                competed = false;
                break;
            }

            if (selections.get("DONW_SUM") != null && !selections.get("DONW_SUM")) {
                competed = false;
                break;
            }
        }

        if (competed) {
            int sumSimpleNumbers = Integer.parseInt(lblAllSumOneToSix.getText());
            int sumMinMax = Integer.parseInt(lblAllSumMaxMin.getText());
            int sumDown = Integer.parseInt(lblAllSumKentaToYamb.getText());

            lblTotalSum.setText(sumSimpleNumbers + sumMinMax + sumDown + "");

            if (PlayerSession.getInstance().getCounter() == 52) {
                try {
                    YambController.getInstance().endGame();
                    Alert a = new Alert(Alert.AlertType.INFORMATION);
                    a.setTitle("Game over");
                    a.setContentText("Your score: " + PlayerSession.getInstance().getScore());
                    a.show();
                    init();
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        }
    }

    private void calculateTotalSimpleNumbers() {
        selectedFields.keySet().forEach(key -> {
            Map<String, Boolean> selections = selectedFields.get(key);
            List<ResultEnum> results = Arrays.asList(ResultEnum.values());
            int sum = 0;
            boolean completed = true;

            for (int i = 0; i < 6; i++) {
                if (selections.get(results.get(i).toString())) {
                    sum += Integer.parseInt(findLabel(key, results.get(i).toString()).getText());
                } else {
                    completed = false;
                }
            }

            if (completed) {
                if (sum > 60) {
                    sum += 30;
                }
                findLabel(key, "SIMPLE_SUM").setText(sum + "");
                selections.put("SIMPLE_SUM", true);
            }

        });

        int total = 0;
        boolean completed = true;

        for (String key : selectedFields.keySet()) {
            Map<String, Boolean> selections = selectedFields.get(key);
            if (selections.get("SIMPLE_SUM") != null && selections.get("SIMPLE_SUM")) {
                total += Integer.parseInt(findLabel(key, "SIMPLE_SUM").getText());
            } else {
                completed = false;
                break;
            }
        }

        if (completed) {
            lblAllSumOneToSix.setText(total + "");
            calculateTotalSum();
        }
    }

    private void calculateMaxMin() {
        selectedFields.keySet().forEach(key -> {
            Map<String, Boolean> selections = selectedFields.get(key);
            List<ResultEnum> results = Arrays.asList(ResultEnum.MAX, ResultEnum.MIN);

            if (selections.get(ResultEnum.ONE.toString())) {
                int sum = 0;
                boolean completed = true;

                if (selections.get(ResultEnum.MAX.toString())) {
                    sum += Integer.parseInt(findLabel(key, ResultEnum.MAX.toString()).getText());
                } else {
                    completed = false;
                }

                if (selections.get(ResultEnum.MIN.toString())) {
                    sum = sum - Integer.parseInt(findLabel(key, ResultEnum.MIN.toString()).getText());
                } else {
                    completed = false;
                }

                if (completed) {
                    findLabel(key, "SUB_MAX_MIN").setText((sum * Integer.parseInt(findLabel(key, ResultEnum.ONE.toString()).getText())) + "");
                    selections.put("SUB_MAX_MIN", true);
                }
            }
        });

        int total = 0;
        boolean completed = true;

        for (String key : selectedFields.keySet()) {
            Map<String, Boolean> selections = selectedFields.get(key);
            if (selections.get("SUB_MAX_MIN") != null && selections.get("SUB_MAX_MIN")) {
                total += Integer.parseInt(findLabel(key, "SUB_MAX_MIN").getText());
            } else {
                completed = false;
                break;
            }
        }

        if (completed) {
            lblAllSumMaxMin.setText(total + "");
            calculateTotalSum();
        }
    }

    private void calculateTotalDownSum() {
        selectedFields.keySet().forEach(key -> {
            Map<String, Boolean> selections = selectedFields.get(key);
            List<ResultEnum> results = Arrays.asList(ResultEnum.values());
            int sum = 0;
            boolean completed = true;

            for (int i = results.indexOf(ResultEnum.KENTA); i < results.indexOf(ResultEnum.YAMB) + 1; i++) {
                if (selections.get(results.get(i).toString())) {
                    sum += Integer.parseInt(findLabel(key, results.get(i).toString()).getText());
                } else {
                    completed = false;
                }
            }

            if (completed) {
                findLabel(key, "DOWN_SUM").setText(sum + "");
                selections.put("DOWN_SUM", true);
            }
        });

        int total = 0;
        boolean completed = true;

        for (String key : selectedFields.keySet()) {
            Map<String, Boolean> selections = selectedFields.get(key);
            if (selections.get("DOWN_SUM") != null && selections.get("DOWN_SUM")) {
                total += Integer.parseInt(findLabel(key, "DOWN_SUM").getText());
            } else {
                completed = false;
                break;
            }
        }

        if (completed) {
            lblAllSumKentaToYamb.setText(total + "");
            calculateTotalSum();
        }
    }

    @FXML
    public void clickOnes(Event event) {
        updateMapValues(event, ResultEnum.ONE);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateTotalSimpleNumbers();
    }

    @FXML
    public void clickTwos(Event event) {
        updateMapValues(event, ResultEnum.TWO);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateTotalSimpleNumbers();
    }

    @FXML
    public void clickThrees(Event event) {
        updateMapValues(event, ResultEnum.THREE);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateTotalSimpleNumbers();
    }

    @FXML
    public void clickFours(Event event) {
        updateMapValues(event, ResultEnum.FOUR);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateTotalSimpleNumbers();
    }

    @FXML
    public void clickFives(Event event) {
        updateMapValues(event, ResultEnum.FIVE);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateTotalSimpleNumbers();
    }

    @FXML
    public void clickSixes(Event event) {
        updateMapValues(event, ResultEnum.SIX);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateTotalSimpleNumbers();
    }

    @FXML
    public void clickMax(Event event) {
        updateMapValues(event, ResultEnum.MAX);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateMaxMin();
    }

    @FXML
    public void clickMin(Event event) {
        updateMapValues(event, ResultEnum.MIN);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateMaxMin();
    }

    @FXML
    public void clickTriling(Event event) {
        updateMapValues(event, ResultEnum.TRILING);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateTotalDownSum();
    }

    @FXML
    public void clickPoker(Event event) {
        updateMapValues(event, ResultEnum.POKER);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateTotalDownSum();
    }

    @FXML
    public void clickKenta(Event event) {
        updateMapValues(event, ResultEnum.KENTA);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateTotalDownSum();
    }

    @FXML
    public void clickFul(Event event) {
        updateMapValues(event, ResultEnum.FUL);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateTotalDownSum();
    }

    @FXML
    public void clickYamb(Event event) {
        updateMapValues(event, ResultEnum.YAMB);
        resetScoreSheet();
        resetSelectedDices();
        resetRollsRectanglesDices();
        calculateTotalDownSum();
    }
}
