/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import communication.YambController;
import forms.FormSwitcher;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author anjak
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("JAMB");
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();

        primaryStage.setOnCloseRequest((event) -> {
            try {
                if (YambController.getInstance().game != null) {
                    YambController.getInstance().endGame();
                }
                YambController.getInstance().exitApp();
            } catch (Exception ex) {
                System.out.println(ex);
            }
        });

        FormSwitcher.setStage(primaryStage);
        FormSwitcher.getInstance().setScene("/forms/login/LoginFXML.fxml");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
