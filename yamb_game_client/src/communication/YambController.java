/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

import domain.Game;
import domain.Player;
import domain.ScorePerGames;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import request.Request;
import request.RequestOperation;
import response.Response;
import response.ResponseStatus;
import utils.PlayerSession;

/**
 *
 * @author anjak
 */
public class YambController {

    private static YambController instance;
    private final SocketCommunication socketCommunication;
    public Game game;

    private YambController() {
        socketCommunication = new SocketCommunication();
    }

    public static YambController getInstance() {
        if (instance == null) {
            instance = new YambController();
        }
        return instance;
    }

    public Player logIn(String username, String password) throws Exception {
        Player player = new Player(username, password);
        Request request = new Request(RequestOperation.LOGIN, player);
        socketCommunication.sendRequest(request);
        Response response = socketCommunication.readResponse();
        if (response.getStatus() == ResponseStatus.SUCCESS) {
            return (Player) response.getPayload();
        }
        throw response.getException();
    }

    public Player signup(String firstname, String lastname, String username, String password) throws Exception {
        Player player = new Player(firstname, lastname, username, password, 0);
        System.out.println("Player: " + player.getFirstname());
        Request request = new Request(RequestOperation.SIGNUP, player);
        socketCommunication.sendRequest(request);
        Response response = socketCommunication.readResponse();
        if (response.getStatus() == ResponseStatus.SUCCESS) {
            return (Player) response.getPayload();
        }
        throw response.getException();
    }

    public Game startGame() throws Exception {
        if (game != null) {
            this.endGame();
        }
        Game r = new Game(PlayerSession.getInstance().getPlayer().getId(), 0, new java.sql.Date(new Date().getTime()));
        Request request = new Request(RequestOperation.START_GAME, r);
        socketCommunication.sendRequest(request);
        Response response = socketCommunication.readResponse();
        if (response.getStatus() == ResponseStatus.SUCCESS) {
            this.game = (Game) response.getPayload();
            PlayerSession.getInstance().setScore(0);
            return this.game;
        }
        throw response.getException();
    }

    public Game endGame() throws Exception {
        this.game.setScore(PlayerSession.getInstance().getScore());

        Player player = PlayerSession.getInstance().getPlayer();
        if (player.getTopScore() < PlayerSession.getInstance().getScore()) {
            player.setTopScore(PlayerSession.getInstance().getScore());
            PlayerSession.getInstance().setPlayer(player);
            update(player);
        }

        Request request = new Request(RequestOperation.END_GAME, game);
        socketCommunication.sendRequest(request);
        Response response = socketCommunication.readResponse();
        if (response.getStatus() == ResponseStatus.SUCCESS) {
            this.game = null;
            return (Game) response.getPayload();
        }
        throw response.getException();
    }

    public void exitApp() throws Exception {
        socketCommunication.exitApp();
    }

    public Player update(Player player) throws Exception {
        Request request = new Request(RequestOperation.UPDATE_PLAYER, player);
        socketCommunication.sendRequest(request);
        Response response = socketCommunication.readResponse();
        if (response.getStatus() == ResponseStatus.SUCCESS) {
            return (Player) response.getPayload();
        }
        throw response.getException();
    }

    public List<Player> getTop10Players(Player player) throws Exception {
        Request request = new Request(RequestOperation.GET_TOP_10_PLAYERS, player);
        socketCommunication.sendRequest(request);
        Response response = socketCommunication.readResponse();
        if (response.getStatus() == ResponseStatus.SUCCESS) {
            return (List<Player>) response.getPayload();
        }
        throw response.getException();
    }

    public List<ScorePerGames> getScorePerGames(ScorePerGames scorePerGames) throws Exception {
        Request request = new Request(RequestOperation.GET_SCORE_PER_GAMES, scorePerGames);
        socketCommunication.sendRequest(request);
        Response response = socketCommunication.readResponse();
        if (response.getStatus() == ResponseStatus.SUCCESS) {
            return (List<ScorePerGames>) response.getPayload();
        }
        throw response.getException();
    }
}
