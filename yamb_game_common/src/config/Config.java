/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package config;

/**
 *
 * @author anjak
 */
public class Config {
    public static final int MAX_CLIENTS = 4;
    public static final int DEFAULT_PORT = 3000;
    public static final String DEFAULT_HOST = "127.0.0.1";
    public static final int MAX_DICE = 5;
    public static final int MAX_ROLLS = 3;
    
}
