/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package request;

import java.io.Serializable;

/**
 *
 * @author anjak
 */
public enum RequestOperation implements Serializable {
    LOGIN,
    SIGNUP,
    START_GAME,
    END_GAME,
    EXIT_APP,
    UPDATE_PLAYER,
    GET_TOP_10_PLAYERS,
    GET_SCORE_PER_GAMES
}
