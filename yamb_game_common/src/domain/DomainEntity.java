/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package domain;

import java.io.Serializable;
import java.sql.ResultSet;

/**
 *
 * @author anjak
 */
public interface DomainEntity extends Serializable {

    String getTableName();

    String getAtributeValues();

    String getAtributeNames();

    String setAtributeValues();

    String getNameByColumn(int i);

    String getWhereCondition();

    DomainEntity getNewRecord(ResultSet rs);
    
    String getShortAttributeNames();
    
    DomainEntity getNewRecordForShortAttributes(ResultSet rs);
    
    String getAttributeNameForSorting();
}
