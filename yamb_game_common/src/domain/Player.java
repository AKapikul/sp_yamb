/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

/**
 *
 * @author anjak
 */
public class Player implements DomainEntity {

    private Long id = 0L;
    private String firstname;
    private String lastname;
    private String username;
    private String password;
    private int topScore;

    public Player(String username) {
        this.username = username;
    }

    public Player(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Player(Long id, String firstname, String lastname, String username, String password, int topScore) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.topScore = topScore;
    }

    public Player(String username, String firstname, String lastname, int topScore) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.topScore = topScore;
    }

    public Player(String firstname, String lastname, String username, String password, int topScore) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.topScore = topScore;
    }

    public Player() {
    }

    public Long getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTopScore() {
        return topScore;
    }

    public void setTopScore(int topScore) {
        this.topScore = topScore;
    }

    @Override
    public String getTableName() {
        return "player";
    }

    @Override
    public String getAtributeValues() {
        StringBuilder sb = new StringBuilder();
        sb.append("'").append(firstname)
                .append("', '").append(lastname)
                .append("', '").append(username)
                .append("', '").append(password)
                .append("', ").append(topScore);
        return sb.toString();
    }

    @Override
    public String getAtributeNames() {
        return "firstname, lastname, username, password, topScore";
    }

    @Override
    public String setAtributeValues() {
        StringBuilder sb = new StringBuilder();
        sb.append("firstname='").append(firstname).
                append("',lastname='").append(lastname).
                append("',username='").append(username).
                append("',password='").append(password).
                append("', topScore=").append(topScore);
        return sb.toString();
    }

    @Override
    public String getNameByColumn(int i) {
        return new String[]{"id", "firstname", "lastname", "username", "password", "topScore"}[i];
    }

    @Override
    public String getWhereCondition() {
        return "username='" + username + "'";
    }

    @Override
    public DomainEntity getNewRecord(ResultSet rs) {
        try {
            long _id = rs.getLong("id");
            String _firstname = rs.getString("firstname");
            String _lastname = rs.getString("lastname");
            String _username = rs.getString("username");
            String _password = rs.getString("password");
            int _topScore = rs.getInt("topScore");

            return new Player(_id, _firstname, _lastname, _username, _password, _topScore);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public int calculateSimpleNumbers(int[] dice, int number) {
        int result = 0;
        for (int i = 0; i < dice.length; i++) {
            if (dice[i] == number) {
                result = result + dice[i];
            }
        }
        return result;
    }

    public int calculateMax(int[] dice) {
        int[] diceCopy = dice.clone();
        Arrays.sort(diceCopy);
        int sum = 0;

        for (int i = 0; i < diceCopy.length; i++) {
            sum += diceCopy[i];
        }

        return sum;
    }

    public int calculateMin(int[] dice) {
        int[] diceCopy = dice.clone();
        Arrays.sort(diceCopy);
        int sum = 0;

        for (int i = 0; i < diceCopy.length; i++) {
            sum += diceCopy[i];
        }

        return sum;
    }

    public int calculateTriling(int[] dice) {
        int[] diceCopy = dice.clone();
        Arrays.sort(diceCopy);

        int sum = 0;
        int sumFinal = 0;
        calculation:
        {
            for (int i = 0; i < diceCopy.length; i++) {
                int counter = 0;
                for (int f = 0; f < diceCopy.length; f++) {
                    if (diceCopy[i] == diceCopy[f]) {
                        counter++;
                        sum = sum + diceCopy[f];
                        if (counter == 3) {
                            sumFinal = sum + 20;
                            break calculation;
                        }
                    } else {
                        sum = 0;
                        counter = 0;
                    }
                }
            }
        }
        return sumFinal;
    }

    public int calculatePoker(int[] dice) {
        int[] diceCopy = dice.clone();
        Arrays.sort(diceCopy);
        int sum = 0;
        int sumFinal = 0;

        calculation:
        {
            for (int i = 0; i < diceCopy.length; i++) {
                int counter = 0;
                for (int f = 0; f < diceCopy.length; f++) {
                    if (diceCopy[i] == diceCopy[f]) {
                        counter++;
                        sum = sum + diceCopy[f];
                        if (counter == 4) {
                            sumFinal = sum + 40;
                            break calculation;
                        }
                    } else {
                        sum = 0;
                        counter = 0;
                    }
                }
            }
        }
        return sumFinal;
    }

    public int calculateKenta(int[] dice, int numberOfRemainingRolls) {
        if (isKenta(dice)) {
            switch (numberOfRemainingRolls) {
                case 2:
                    return 66;
                case 1:
                    return 56;
                case 0:
                    return 46;
                default:
                    return 0;
            }
        }

        return 0;
    }

    public int calculateFul(int[] dice) {
        if (isFull(dice)) {
            int sum = 0;
            for (int i = 0; i < dice.length; i++) {
                sum += dice[i];
            }
            return sum + 30;
        } else {
            return 0;
        }
    }

    public int calculateYamb(int[] dice) {
        int[] diceCopy = dice.clone();
        Arrays.sort(diceCopy);
        int sumFinal = 0;
        calculation:
        {
            for (int i = 0; i < diceCopy.length; i++) {
                int counter = 0;
                for (int f = 0; f < diceCopy.length; f++) {
                    if (diceCopy[i] == diceCopy[f]) {
                        counter++;
                        if ((counter == 5) && (diceCopy[i] != 0)) {
                            sumFinal = diceCopy[i] * 5 + 50;
                            break calculation;
                        }
                    } else {
                        break calculation;
                    }
                }
            }
        }
        return sumFinal;
    }

    public static boolean isTwoPairs(int[] dice) {
        if ((!isPoker(dice)) && (!isYamb(dice))) {
            int[] diceCopy = dice.clone();
            Arrays.sort(diceCopy);
            int prev = 0;
            int pairs = 0;

            for (int i = 0; i < diceCopy.length; i++) {
                int current = diceCopy[i];
                if (current == prev) {
                    pairs++;
                    prev = 0;
                } else {
                    prev = current;
                }
            }
            if (pairs == 2) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean isTriling(int[] dice) {
        int[] diceCopy = dice.clone();
        Arrays.sort(diceCopy);
        boolean value = false;
        checkingBlock:
        {
            for (int i = 0; i < diceCopy.length; i++) {
                int counter = 0;
                for (int f = 0; f < diceCopy.length; f++) {
                    if (diceCopy[i] == diceCopy[f]) {
                        counter++;
                        if (counter == 3) {
                            value = true;
                        }
                        if (counter == 4) {
                            value = false;
                        }
                        if (counter == 5) {
                            value = false;
                            break checkingBlock;
                        }
                    }
                }
            }
        }
        return value;
    }

    public static boolean isPoker(int[] dice) {
        int[] diceCopy = dice.clone();
        Arrays.sort(diceCopy);

        boolean value = false;
        checkingBlock:
        {
            for (int i = 0; i < diceCopy.length; i++) {
                int counter = 0;
                for (int f = 0; f < diceCopy.length; f++) {
                    if (diceCopy[i] == diceCopy[f]) {
                        counter++;
                        if (counter == 4) {
                            value = true;
                        }
                        if (counter == 5) {
                            value = false;
                            break checkingBlock;
                        }
                    }
                }
            }
        }
        return value;
    }

    public static boolean isKenta(int[] dice) {
        int[] diceCopy = dice.clone();
        Arrays.sort(diceCopy);
        boolean condition = false;

        checkingBlock:
        {
            switch (diceCopy[0]) {
                case 1:
                    for (int i = 0; i < diceCopy.length - 1; i++) {
                        if (diceCopy[i + 1] == diceCopy[i] + 1) {
                            condition = true;
                        } else {
                            condition = false;
                            break checkingBlock;
                        }
                    }
                    break;
                case 2:
                    for (int i = 0; i < diceCopy.length - 1; i++) {
                        if (diceCopy[i + 1] == diceCopy[i] + 1) {
                            condition = true;
                        } else {
                            condition = false;
                            break checkingBlock;
                        }
                    }
                    break;
                default:
                    condition = false;
                    break checkingBlock;
            }
        }
        return condition;
    }

    public static boolean isFull(int[] dice) {
        if ((isTriling(dice)) && (isTwoPairs(dice))) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isYamb(int[] dice) {
        int[] diceCopy = dice.clone();
        Arrays.sort(diceCopy);

        boolean value = false;
        checkingBlock:
        {
            for (int i = 0; i < diceCopy.length; i++) {
                int counter = 0;
                for (int f = 0; f < diceCopy.length; f++) {
                    if (diceCopy[i] == diceCopy[f]) {
                        counter++;
                        if (counter == 5) {
                            value = true;
                            break checkingBlock;
                        }
                    }
                }
            }
        }
        return value;
    }

    @Override
    public String getShortAttributeNames() {
        return "username, firstname, lastname, topScore";
    }

    @Override
    public DomainEntity getNewRecordForShortAttributes(ResultSet rs) {
        try {
            String _firstname = rs.getString("firstname");
            String _lastname = rs.getString("lastname");
            String _username = rs.getString("username");
            int _topScore = rs.getInt("topScore");

            return new Player(_username, _firstname, _lastname, _topScore);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public String getAttributeNameForSorting() {
        return "topScore";
    }
}
