/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author anjak
 */
public class Game implements DomainEntity {

    private Long id = -1L;
    private Long playerId = -1L;
    private int score = -1;
    private Date date;

    public Game() {
    }

    public Game(Long id) {
        this.id = id;
    }

    public Game(Long playerId, int score, Date date) {
        this.playerId = playerId;
        this.score = score;
        this.date = date;
    }

    public Game(Long id, Long playerId, int score, Date date) {
        this.id = id;
        this.playerId = playerId;
        this.score = score;
        this.date = date;
    }

    @Override
    public String getTableName() {
        return "game";
    }

    @Override
    public String getAtributeValues() {
        StringBuilder sb = new StringBuilder();
        sb.append("").append(playerId)
                .append(", ").append(score)
                .append(", '").append(new Timestamp(date.getTime()))
                .append("'");
        return sb.toString();
    }

    @Override
    public String getAtributeNames() {
        return "playerId, score, date";
    }

    @Override
    public String setAtributeValues() {
        StringBuilder sb = new StringBuilder();
        sb.append("playerId=").append(playerId).
                append(",score=").append(score)
                .append(",date='").append(date).append("'");
        return sb.toString();
    }

    @Override
    public String getNameByColumn(int i) {
        return getAtributeNames().split(",")[i];
    }

    @Override
    public String getWhereCondition() {
        return "id=" + id;
    }

    @Override
    public DomainEntity getNewRecord(ResultSet rs) {
        try {
            Long _id = rs.getLong("id");
            Long _playerId = rs.getLong("playerId");
            int _score = rs.getInt("score");
            Date _date = rs.getDate("date");
            return new Game(_id, _playerId, _score, _date);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.playerId);
        hash = 97 * hash + this.score;
        hash = 97 * hash + Objects.hashCode(this.date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Game other = (Game) obj;
        if (this.score != other.score) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.playerId, other.playerId)) {
            return false;
        }
        return Objects.equals(this.date, other.date);
    }

    @Override
    public String toString() {
        return "Game{" + "id=" + id + ", playerId=" + playerId + ", score=" + score + ", date=" + date + '}';
    }

    @Override
    public String getShortAttributeNames() {
        return "*";
    }

    @Override
    public DomainEntity getNewRecordForShortAttributes(ResultSet rs) {
        return null;
    }

    @Override
    public String getAttributeNameForSorting() {
        return "score";
    }

}
