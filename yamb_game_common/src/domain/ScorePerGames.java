/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package domain;

import java.sql.ResultSet;
import java.util.Date;

/**
 *
 * @author anjak
 */
public class ScorePerGames implements DomainEntity {

    private String username;
    private Long score;
    private Date date;

    public ScorePerGames(String username, Long score, Date date) {
        this.username = username;
        this.score = score;
        this.date = date;
    }

    public ScorePerGames() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String getTableName() {
        return "";
    }

    @Override
    public String getAtributeValues() {
        return "";
    }

    @Override
    public String getAtributeNames() {
        return "";
    }

    @Override
    public String setAtributeValues() {
        return "";
    }

    @Override
    public String getNameByColumn(int i) {
        return "";
    }

    @Override
    public String getWhereCondition() {
        return "";
    }

    @Override
    public DomainEntity getNewRecord(ResultSet rs) {
        return null;
    }

    @Override
    public String getShortAttributeNames() {
        return "";
    }

    @Override
    public DomainEntity getNewRecordForShortAttributes(ResultSet rs) {
        return null;
    }

    @Override
    public String getAttributeNameForSorting() {
        return "";
    }

}
